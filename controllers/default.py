# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

import json
import iso8601
import pytz

@requires_login
def index():
    """
    List of student check-ins.
    """
    if not is_user_admin():
        redirect(URL('default', 'denied'))
    q = db.checkin
    grid = SQLFORM.grid(q,
        csv=True,
        details=False, create=False,
        editable=False,
        deletable=False,
        paginate=40,
        )
    return dict(grid=grid)

def put():
    """Puts some data into the storage."""
    app_id = request.vars.app_id
    key = request.vars.key
    data = request.vars.data
    db.storage.insert(app_id=app_id, key=key, data=data)
    return response.json("ok")

def get():
    """Reads data from the storage."""
    app_id = request.vars.app_id
    key = request.vars.key
    r = db((db.storage.app_id == app_id) & (db.storage.key == key)).select().first()
    return response.json(r.data if r is not None else None)   

def post_msg():
    """Posts a message, and returns the list of last results."""
    app_id = request.vars.app_id
    msg = request.vars.msg
    db.bboard.insert(app_id=app_id, message=msg)    
    l = get_latest_posts(app_id)
    return response.json(dict(messages=l))

def get_msg():
    app_id = request.vars.app_id
    l = get_latest_posts(app_id)
    return response.json(dict(messages=l))

def get_latest_posts(app_id):
    rows = db(db.bboard.app_id == app_id).select() # orderby=~db.bboard.post_date), limitby=(0,10))
    l = []
    for r in rows:
        if len(l) < 10:
            l.append(r.message)
        else:
            break
    return l


####
# The following functions are for the homework on local messages.
NUM_LOCAL_MSGS = 10
import heapq

def get_local():
    lat = float(request.vars.lat)
    lng = float(request.vars.lng)
    return response.json(dict(messages=get_local_messages(lat, lng)))

def put_local():
    lat = float(request.vars.lat)
    lng = float(request.vars.lng)
    msgid = request.vars.msgid
    userid = request.vars.userid
    msg = request.vars.msg
    pt = pt_to_grid((lat, lng))
    db.local_messages.insert(msgid=msgid, userid=userid, latlng=pt, msg=msg)
    return response.json(dict(messages=get_local_messages(lat, lng)))    

def get_local_messages(lat, lng):
    """Returns the latest messages at a location."""
    # Finds points around the location.
    pts = [(lat - 0.0004, lng - 0.0004),
           (lat - 0.0004, lng + 0.0004),
           (lat + 0.0004, lng - 0.0004),
           (lat + 0.0004, lng + 0.0004)]
    squares = set([pt_to_grid(p) for p in pts])
    # Retrieves messages for each square. 
    messages = flatten([get_messages_in_square(q, NUM_LOCAL_MSGS) for q in squares])
    # Now gets the most recent.
    most_recent = heapq.nlargest(NUM_LOCAL_MSGS, messages)
    return [dict(ts=ts.isoformat(), msgid=msgid, userid=userid, msg=msg) for (ts, userid, msgid, msg) in most_recent]

def pt_to_grid(pt):
    """Transforms a point into a string for retrieval."""
    x, y = pt
    sx = str(int(x * 1000))
    sy = str(int(y * 1000))
    return sx + "," + sy

def flatten(l):
    return [item for sublist in l for item in sublist]

def get_messages_in_square(q, howmany):
    rows = db(db.local_messages.latlng == q).select(orderby=~db.local_messages.timestamp, limitby=(0,howmany))
    return [(r.timestamp, r.userid, r.msgid, r.msg) for r in rows]

def local_index():
    """Visualizes for the student the local messages."""
    grid = SQLFORM.grid(db.local_messages,
        editable=False, deletable=False, csv=False)
    return dict(grid=grid)


#####

@requires_login
def index():
    """
    List of student check-ins.
    """
    if not is_user_admin():
        redirect(URL('default', 'denied'))
    q = db.checkin
    grid = SQLFORM.grid(q,
        csv=True,
        details=False, create=False,
        editable=False,
        deletable=False,
        paginate=40,
        )
    return dict(grid=grid)

SIGNING_KEY = "potatoes";

def checkin():
    """This controller is used to add a checkin from a student."""
    json_info = request.vars.payload
    sig = request.vars.sig
    info = ""
    if json_info is not None:
        info = json.loads(json_info)
    # We just log to make sure.
    logger.info("Json_info: %r sig: %r info: %r" % (json_info, sig, info))
    if sig == SIGNING_KEY:
        logger.info("Valid signature; inserting")
        ts = iso8601.parse_date(info['timeStamp'])
        ts = ts.replace(tzinfo=None)
        logger.info("Received time: %r decoded time: %r" % (info['timeStamp'], ts))
        lat = info.get('lat')
        lng = info.get('lng')
        grid_id = str(int(lat * 100)) + "," + str(int(lng * 100))
        db.checkin.insert(user=info['account'], lat=lat, lng=lng, grid_id=grid_id, accuracy=info.get('accuracy'), timestamp=ts)
        return response.json("ok")
    else:
        return response.json("no")


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


